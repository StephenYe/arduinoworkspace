int trigPIN = 9;
int echoPIN = 10;

long duration;
int distance;

void setup() {
  // put your setup code here, to run once:
  pinMode(trigPIN, OUTPUT);
  pinMode(echoPIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  //clear the trigPIN
  digitalWrite(trigPIN, LOW);
  delayMicroseconds(2);

  //Set the trigPIN on HIGH state for 10ms
  digitalWrite(trigPIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPIN, LOW);

  //Read the echoPIN, returns the sound wave
  duration = pulseIn(echoPIN, HIGH);
  //calculate the distance
  distance = duration * 0.034/2;

  Serial.print("Distance:");
  Serial.println(distance);
}
